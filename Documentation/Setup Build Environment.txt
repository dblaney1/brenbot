##################################################################################
#
# Setting up a BRenBot 1.x development and build environment
#
# Author: danpaul88
# Revision: 1.10
# Date: 11-08-2013
#
##################################################################################


1) Download and install Strawberry Perl 5.18.2 (32 bit) from

   http://strawberry-perl.googlecode.com/files/strawberry-perl-5.18.2.2-32bit.msi




2) After installing Strawberry Perl open a command prompt (WinKey+R, cmd, enter) and type in cpan
   and hit the enter key. Once it loads type the following commands

   install POE::Test::Loops
   install POE
   install Date::Calc
   install Class::Unload
   install pp

   Once all of these commands are completed you should be able to run brenbot.pl successfully and
   compile it into a distributable package using the instructions in Packaging Command Lines.txt